    <header id="home" class="{{ Request::is('/') ? 'welcome-area' : 'blog-page-design-area' }}">
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <!-- START LOGO DESIGN AREA -->
                        <div class="logo">
                            <a href="{{ route('home')}}">Federico</a>
                        </div>
                        <!-- END LOGO DESIGN AREA -->
                    </div>
                    <div class="col-sm-9">
                        <!-- START MENU DESIGN AREA -->
                        <div class="mainmenu">
                            <div class="navbar navbar-nobg">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active"><a class="smoth-scroll" href="{{ route('home') . '/#home'}}">Home <div class="ripple-wrapper"></div></a>
                                        </li>
                                        <li><a class="smoth-scroll" href="{{ route('home') . '/#about'}}">About</a>
                                        </li>
                                        <li><a class="smoth-scroll" href="{{ route('home') . '/#service'}}">service</a>
                                        </li>
                                        <li><a class="smoth-scroll" href="{{ route('home') . '/#work'}}">Work</a>
                                        </li>
                                        <li><a class="smoth-scroll" href="{{ route('home') . '/#quotes'}}">Quotes</a>
                                        </li>
                                        <li><a class="smoth-scroll" href="{{ route('home') . '/#contact'}}">Contact</a>
                                        </li>
                                        <li><a class="smoth-scroll" href="{{ route('blog')}}">Blog</a>
                                        </li>
                                        @if(Auth::check())
                                            <li><a class="smoth-scroll user" href="{{ route('home')}}">{{ Auth::user()->name }}</a>
                                        @endif
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- END MENU DESIGN AREA -->
                    </div>
                </div>
            </div>
        </div>
        @if (Request::is('/'))
            <div class="welcome-image-area particle-bg" data-stellar-background-ratio="0.6">
                <div id="particles-js"></div>
                <div class="display-table">
                    <div class="display-table-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="header-text text-center">
                                        <h1 class="typewrite" data-period="2000" data-type='[ "Hi, I am Federico.", "I am Programmer.", "I love to create.", "I am from Sicily." ]'>
                                            <span class="wrap"></span>
                                        </h1>
                                        <div class="social-links header-links">
                                            <ul>
                                                <li>
                                                    <a href="https://linkedin.com/in/federicoboncaldo" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="home-arrow-down">
                                            <a href="#about" class="smoth-scroll btn"><i class="fa fa-angle-double-down"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (Request::is('/blog'))
            <div class="blog-page-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>post details</h2>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </header>