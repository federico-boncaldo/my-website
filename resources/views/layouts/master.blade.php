<!DOCTYPE html>
<html lang="en">

<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Personal Portfolio Template">
    <meta name="keywords" content="creative personal, resume, cv, portfolio, personal, developer, Monster,personal resume, onepage, clean, modern">
    <meta name="author" content="Tanvir Rahman Hridoy">
    <!-- PAGE TITLE -->
    <title>@yield('title') - Federico Boncaldo</title>
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <!-- ALL GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Montserrat:300,400,500,600,700,800,900|Dancing+Script:400,700&subset=latin-ext" rel="stylesheet">
    <!-- FONT AWESOME CSS -->
    <link rel="stylesheet" href="/assets/fonts/font-awesome.min.css">
    <!-- OWL CAROSEL CSS -->
    <link rel="stylesheet" href="/assets/owlcarousel/css/owl.carousel.css">
    <link rel="stylesheet" href="/assets/owlcarousel/css/owl.theme.css">
    <!-- MAGNIFIC CSS -->
    <link rel="stylesheet" href="/assets/css/magnific-popup.css">
    <!-- ANIMATE CSS -->
    <link rel="stylesheet" href="/assets/css/animate.min.css">
    <link rel="stylesheet" href="/assets/css/effects.css">
    <link rel="stylesheet" href="/assets/css/YTPlayer.css">
    <!-- MAIN STYLE CSS -->
    <link rel="stylesheet" href="/assets/css/style.css">
    <!-- RESPONSIVE CSS -->
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- START PRELOADER -->
    <div class="preloader">
        <div class="status">
            <div class="status-mes"></div>
        </div>
    </div>
    <!-- / END PRELOADER -->

    @include('layouts.header')

    @yield ('content')

    @include('layouts.footer')

</body>
</html>