@extends ('layouts.master')

@section  ('content')
    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Register</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form id="contactForm" class="contact-form" action="/register" method="post">
                    	{{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6 form-group">
                            	<label for="name">Name:</label>
                                <input id="name" class="form-control" type="text" name="name" placeholder="Name">
                            </div>
                            <div class="col-md-6 form-group">
                            	<label for="email">Email:</label>
                                <input id="email" class="form-control" type="email" name="email" placeholder="Email">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="password">Password:</label>
                                <input id="password" class="form-control" type="password" name="password" placeholder="Password">
                            </div>
                            <div class="col-md-6 form-group">
                            	<label for="password">Password confirmation:</label>
                                <input id="password_confirmation" class="form-control" type="password" name="password_confirmation" placeholder="Password confirmation">
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Register</button>
                            </div>
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->
@endsection