@extends ('layouts.master')

@section('title' , 'Home')

@section ('content')
    <!-- START ABOUT DESIGN AREA -->
    <section id="about" class="about-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>about Federico</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!-- START ABOUT IMAGE AREA -->
                    <div class="about-image">
                        <img src="assets/images/me.jpg" alt="">
                    </div>
                    <!-- / END ABOUT IMAGE AREA -->
                </div>
                <div class="col-md-6">
                    <!-- START ABOUT TEXT AREA -->
                    <div class="about-text">
                        @if(!empty($about))
                            <h2 class="wow fadeInUp" data-wow-delay="0.4s">{{ $about->title }}</h2>
                            <p class="wow fadeInUp" data-wow-delay="0.6s">{{ $about->body }}</p>
                        @endif

                        @if(!empty($services))
                        <ul class="wow fadeInUp" data-wow-delay="0.8s">
                            @foreach($services as $service)
                                <li>{{ $service->title}}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                    <!-- / END ABOUT TEXT AREA -->
                    @if(Auth::check())
                        <div class="row">
                            <div class="col-sm-12">
                                @if(empty($about))
                                    <a class="smoth-scroll user" href="{{ route('about.create')}}">Create About Us</a>
                                @else
                                    <a class="smoth-scroll user" href="{{ route('about.edit', $about->id )}}">Edit</a>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- / END ABOUT DESIGN AREA -->


    <!-- START SERVICES DESIGN AREA -->
    <section id="service" class="service-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>what i offer?</h2>
                    </div>
                </div>
            </div>
            <div class="row">

                @if(!empty($services))
                    @foreach($services as $service)
                        <!-- START SINGLE SERVICE DESIGN AREA -->
                        <div class="col-md-6 col-sm-6">
                            <div class="single-service wow fadeInUp" data-wow-delay="0.2s">
                                <i class="fa fa-{{$service->icon}}"></i>
                                    <h2><a href="/services/{{ $service->id }}">{{ $service->title }}</a></h2>
                                    <p>{{ $service->body }}</p>
                                    @if(Auth::check())
                                        <a class="smoth-scroll user" href="{{ route('service.edit', $service->id )}}">Edit</a>
                                    @endif
                            </div>
                        </div>
                        <!-- / END SINGLE SERVICE DESIGN AREA -->
                    @endforeach
                @endif
            </div>
            @if(Auth::check())
                <div class="row">
                    <div class="col-sm-12">
                    <a href="/services/create">Create a new service</a>
                    </div>
                </div>
            @endif
        </div>
    </section>
    <!-- / END SERVICES DESIGN AREA -->


        <!-- START WORK DESIGN AREA -->
    <section id="work" class="work section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>featured works.</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <ul class="work">
                    <li class="filter" data-filter="all">all</li>
                    @foreach($categories as $category)
                        <li class="filter" data-filter=".{{ preg_replace('/\s+/', '', $category->title) }}">{{$category->title}}</li>
                    @endforeach
                </ul>
            </div>
            <div class="work-inner">
                <div class="row work-posts grid">
                    <!-- START SINGLE WORK DESIGN AREA -->
                    @foreach($categories as $category)
                        @foreach($category->works as $work)
                            <div class="col-md-4 col-sm-6 mix {{ preg_replace('/\s+/', '', $category->title) }}">
                                <div class="item">
                                    <a href="assets/images/work/1.jpg" class="work-popup">
                                        <figure class="effect-ruby">
                                            <img src="assets/images/work/1.jpg" alt="image">
                                            <figcaption>
                                                <h2><a href="/works/{{ $work->id }}">{{ $work->title }}</a></h2>
                                                <p>project one</p>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        @if(Auth::check())
                            <a class="smoth-scroll user" href="{{ route('work-categories.edit', $category->id )}}">Edit {{ $category->title }}</a>
                            <a class="smoth-scroll user" href="{{ route('works.create', $category->id )}}">Create a new work in {{ $category->title }}</a>
                        @endif
                    @endforeach
                    <div class="col-md-4 col-sm-6 mix ">
                        <div class="item">
                            <a href="assets/images/work/1.jpg" class="work-popup">
                                <figure class="effect-ruby">
                                    <img src="assets/images/work/1.jpg" alt="image">
                                    <figcaption>
                                        <h2>development</h2>
                                        <p>project one</p>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                </div>
                @if(Auth::check())
                    <a class="smoth-scroll user" href="{{ route('work-categories.create') }}">Create new category</a>
                @endif
            </div>
        </div>
    </section>
    <!-- / END START WORK DESIGN AREA -->



    <!-- START QUOTES DESIGN AREA -->
    <section id="quotes" class="testimonial-area section-padding">
        <div class="container">
            @if(!empty($quotes))
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h2>Quotes!</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="testimonial-list">
                            <!-- START SINGLE TESTIMONIAL DESIGN AREA -->
                            @foreach($quotes as $quote)
                                <div class="single-testimonial text-center">
                                    <p>{{ $quote->body}}</p>
                                    <h2>{{ $quote->author }}</h2>
                                    <h3>{{ $quote->title }}</h3>
                                    @if(Auth::check())
                                        <a href="/quotes/edit/{{$quote->id}}">Edit</a>
                                    @endif
                                </div>
                            @endforeach
                            <!-- / END SINGLE TESTIMONIAL DESIGN AREA -->
                        </div>
                    </div>
                </div>
            @endif

            @if(Auth::check())
                <div class="row">
                    <div class="col-sm-12">
                        <a href="/quotes/create" >Create a new quote</a>
                    </div>
                </div>
            @endif
        </div>
    </section>
    <!-- / END QUOTES DESIGN AREA -->

    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Get in touch</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form id="contactForm" class="contact-form" action="/contact" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input id="name" class="form-control" type="text" name="name" placeholder="Name">
                            </div>
                            <div class="col-md-6 form-group">
                                <input id="email" class="form-control" type="email" name="email" placeholder="Email">
                            </div>
                            <div class="col-md-12 form-group">
                                <input id="subject" class="form-control" type="text" name="subject" placeholder="Subject">
                            </div>
                            <div class="col-md-12 form-group">
                                <textarea id="message" class="form-control" placeholder="Message" rows="4" ></textarea>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Submit Now</button>
                            </div>
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->
@endsection ('content')