@extends('layouts.master')

@section('title' , 'About Us - Edit')

@section('content')
    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>About</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form id="contactForm" class="contact-form" action="/about/{{$about->id}}" method="post">
                        @method("PATCH")
                        @csrf
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="title">Title:</label>
                                <input id="title" class="form-control" type="text" name="title" placeholder="Title" value="{{$about->title}}">
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="body">Body:</label>
                                <textarea id="body" class="form-control" type="textarea" name="body" placeholder="Body" >{{$about->body}}</textarea>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Update</button>
                            </div>
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                        </div>
                    </form>
                    <form id="deleteForm" class="contact-form" action="/about/{{$about->id}}" method="post">
                        @method("DELETE")
                        @csrf
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Delete</button>
                            </div>
                        </div>
                    </form>
                    @include('layouts.home-button')
                </div>
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->
@endsection