@extends('layouts.master')

@section('title' ,"{$workCategory->title} - Edit")

@section('content')
    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>{{ $workCategory->title }}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form id="contactForm" class="contact-form edit-form" action="/works/category/{{ $workCategory->id }}" method="post">
                        @method("PATCH")
                        @csrf
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="title">Title:</label>
                                <input id="title" class="form-control" type="text" name="title" placeholder="{{ $workCategory->title }}" value="{{ $workCategory->title }}">
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                    <form id="deleteForm" class="contact-form" action="/works/category/{{ $workCategory->id }}" method="post">
                        @method("DELETE")
                        @csrf
                        <div class="col-md-12 text-center">
                            <button class="btn" type="submit">Delete</button>
                        </div>
                    </form>
                    @include('layouts.home-button')
                </div>
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->
@endsection