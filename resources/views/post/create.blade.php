@extends('layouts.master')

@section('content')
    <!-- START BLOG POST DESIGN AREA -->
    <section class="blog-category section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                     <form id="contactForm" class="contact-form" action="/blog" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="title">Title:</label>
                                <input id="title" class="form-control" type="text" name="title" placeholder="Title">
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="body">Body:</label>
                                <textarea id="body" class="form-control" type="textarea" name="body" placeholder="Body"></textarea>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Submit</button>
                            </div>
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                        </div>
                    </form>
                    @include('layouts.home-button')
               </div>
               @include ('layouts.sidebar')
            </div>
        </div>
    </section>
    <!-- / END BLOG POST DESIGN AREA -->
@endsection