@extends ('layouts.master')

@section('title' , 'Blog')

@section ('content')
    <!-- START BLOG POST DESIGN AREA -->
    <section class="blog-category section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <!-- SINGLE BLOG POST DESIGN AREA -->
                    <div class="single-blog-post wow fadeInDown" data-wow-delay="0.2s">
                        <img src="assets/images/blog/blog1.jpg" class="img-responsive" alt="" />
                        <h2>Create website step by step</h2>
                        <div class="post-date">
                            <span><i class="fa fa-calendar"></i> 02,Feb 2017</span>
                            <span><i class="fa fa-folder-open-o"></i> website development</span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.</p>
                        <a href="{{ route('post', 1) }}" class="read-more hvr-sweep-to-top">Read more</a>
                    </div>
                    <!-- / END BLOG POST DESIGN AREA -->


                    <!-- SINGLE BLOG POST DESIGN AREA -->
                    <div class="single-blog-post wow fadeInDown" data-wow-delay="0.4s">

                        <div class="blog-slider">
                            <div class="single-blog-slider">
                                <img src="assets/images/blog/blog3.jpg" class="img-responsive" alt="" />
                            </div>
                            <div class="single-blog-slider">
                                <img src="assets/images/blog/blog2.jpg" class="img-responsive" alt="" />
                            </div>
                        </div>

                        <h2>Make website step by step</h2>
                        <div class="post-date">
                            <span><i class="fa fa-calendar"></i> 02,Feb 2017</span>
                            <span><i class="fa fa-folder-open-o"></i> website development</span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.</p>
                        <a href="single-blog.html" class="read-more hvr-sweep-to-top">Read more</a>
                    </div>
                    <!-- / END BLOG POST DESIGN AREA -->


                    <!-- SINGLE BLOG POST DESIGN AREA -->
                    <div class="single-blog-post wow fadeInDown" data-wow-delay="0.6s">
                        <div class="video-area">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe width="800" height="500" src="https://www.youtube.com/embed/Yp5RloYcdic?rel=0&amp;controls=0&amp;showinfo=0&autoplay=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <h2>Build website step by step</h2>
                        <div class="post-date">
                            <span><i class="fa fa-calendar"></i> 02,Feb 2017</span>
                            <span><i class="fa fa-folder-open-o"></i> website development</span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.</p>
                        <a href="{{ route('post', 1) }}" class="read-more hvr-sweep-to-top">Read more</a>
                    </div>
                    <!-- / END BLOG POST DESIGN AREA -->
                    <!-- START BLOG POST PAGINATION DESIGN AREA -->
                    <div id="pagination">
                        <nav>
                            <ul class="pagination blog_pagination">
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li>
                                    <a href="#" class="black-icon" aria-label="Next">
                                        <span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- / END BLOG POST PAGINATION DESIGN AREA -->
                </div>
                @include ('layouts.sidebar')
            </div>
        </div>
    </section>
    <!-- / END BLOG POST DESIGN AREA -->
@endsection