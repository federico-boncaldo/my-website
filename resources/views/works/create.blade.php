@extends('layouts.master')

@section('title' , 'Works - Create')

@section('content')
    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Works</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form id="contactForm" class="contact-form" action="/works" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="title">Title:</label>
                                <input id="title" class="form-control" type="text" name="title" placeholder="Title">
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="category_id">Category:</label>
                                <input id="category_id" class="form-control" type="select" name="category_id" placeholder="Category" value="{{ $category }}">
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Submit</button>
                            </div>
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                        </div>
                    </form>
                </div>
                @include('layouts.home-button')
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->
@endsection