@extends('layouts.master')

@section('title' , "$work->title")

@section('content')
    <!-- START BLOG POST DESIGN AREA -->
    <section class="blog-category section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <!-- SINGLE BLOG POST DETAILS DESIGN AREA -->
                    <div class="single-blog-post-details wow fadeInDown" data-wow-delay="0.2s">
                        <h2>{{ $work->title }}</h2>
                        <p>{{ $work->category_id }}</h2>
                        <p>{{ $work->body }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a class="btn" href="/works/edit/{{ $work->id }}">Edit</a>
            </div>
            @include('layouts.home-button')
        </div>
    </section>
    <!-- / END BLOG POST DESIGN AREA -->
@endsection