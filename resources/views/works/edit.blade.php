@extends('layouts.master')

@section('title' ,"{$work->title} - Edit")

@section('content')
    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>{{ $work->title }}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form id="contactForm" class="contact-form edit-form" action="/works/{{ $work->id }}" method="post">
                        @method("PATCH")
                        @csrf
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="title">Title:</label>
                                <input id="title" class="form-control" type="text" name="title" placeholder="Title" value="{{ $work->title }}">
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="category">Category</label>
                                <input id="category" class="form-control" type="text" name="category" placeholder="category" value="{{ $work->category_id }}" required>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                    <form id="deleteForm" class="contact-form" action="/works/{{ $work->id }}" method="post">
                        @method("DELETE")
                        @csrf
                        <div class="col-md-12 text-center">
                            <button class="btn" type="submit">Delete</button>
                        </div>
                    </form>
                    @include('layouts.home-button')
                </div>
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->
@endsection