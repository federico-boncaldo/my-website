@component ('mail::message')

Hi {{ $name }},

Thanks very much for getting in touch<br>

I'll get back to you as soon as I can<br>

Best,

Federico

@endcomponent