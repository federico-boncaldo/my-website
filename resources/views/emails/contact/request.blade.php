@component ('mail::message')

Dear Federico,

You have just received a message from {{ $request['name'] }} {{ $request['lastname'] }}.<br>

Email address: {{ $request['email'] }}<br>
Subject: {{ $request['subject'] }}<br>
Message: {{ $request['message'] }}<br>


@endcomponent