@extends('layouts.master')

@section('title' ,"{$quote->title} - Edit")

@section('content')
    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>{{ $quote->title }}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form id="contactForm" class="contact-form edit-form" action="/quotes/{{ $quote->id }}" method="post">
                        @method("PATCH")
                        @csrf
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="author">Author:</label>
                                <input id="author" class="form-control" type="text" name="author" placeholder="author" value="{{ $quote->author }}">
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="author_title">Author title:</label>
                                <input id="author_title" class="form-control" type="text" name="author_title" placeholder="Author title" value="{{ $quote->author_title }}">
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="body">Body:</label>
                                <textarea id="body" class="form-control" type="textarea" name="body" placeholder="Body">{{ $quote->body }}</textarea>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                    <form id="deleteForm" class="contact-form" action="/quotes/{{ $quote->id }}" method="post">
                        @method("DELETE")
                        @csrf
                        <div class="col-md-12 text-center">
                            <button class="btn" type="submit">Delete</button>
                        </div>
                    </form>
                    @include('layouts.home-button')
                </div>
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->
@endsection