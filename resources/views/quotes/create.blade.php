@extends('layouts.master')

@section('title' , 'Quotes - Create')

@section('content')
    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Quotes</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form id="contactForm" class="contact-form" action="/quotes" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="title">Author:</label>
                                <input id="author" class="form-control" type="text" name="author" placeholder="Author" value="{{old('author')}}" required>
                            </div>
                            <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="author_title">Author title:</label>
                                <input id="author_title" class="form-control" type="text" name="author_title" placeholder="Author title" value="{{old('author_title')}}" required>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="body">Body:</label>
                                <textarea id="body" class="form-control" type="textarea" name="body" placeholder="Body" required>{{ old('body') }}</textarea>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Create</button>
                            </div>
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                        </div>
                        @include('layouts.home-button')
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->
@endsection