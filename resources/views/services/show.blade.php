@extends('layouts.master')

@section('title' , "$service->title")

@section('content')
    <!-- START BLOG POST DESIGN AREA -->
    <section class="blog-category section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <!-- SINGLE BLOG POST DETAILS DESIGN AREA -->
                    <div class="single-blog-post-details wow fadeInDown" data-wow-delay="0.2s">
                        <img src="assets/images/blog/blog1.jpg" class="img-responsive" alt="" />
                        <h2>{{ $service->title }}</h2>
                        <p>{{ $service->body }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a class="btn" href="/services/edit/{{ $service->id }}">Edit</a>
            </div>
            @include('layouts.home-button')
        </div>
    </section>
    <!-- / END BLOG POST DESIGN AREA -->
@endsection