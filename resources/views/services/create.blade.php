@extends('layouts.master')

@section('title' , 'Services - Create')

@section('content')
    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Services</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form id="contactForm" class="contact-form" action="/services" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="title">Title:</label>
                                <input id="title" class="form-control" type="text" name="title" placeholder="Title" value="{{old('title')}}" required>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="title">Font awesome Icon <a href="https://fontawesome.com/icons?d=gallery" target="_blank">(check the icons here)</a>:</label>
                                <input id="icon" class="form-control" type="text" name="icon" placeholder="Icon" value="{{old('icon')}}" required>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="body">Body:</label>
                                <textarea id="body" class="form-control" type="textarea" name="body" placeholder="Body" required>{{ old('body') }}</textarea>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Create</button>
                            </div>
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                        </div>
                    </form>
                    @include('layouts.home-button')
                </div>
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->
@endsection