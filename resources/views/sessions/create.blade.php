@extends ('layouts.master')

@section ('title', 'Sign in')

@section  ('content')
    <!-- START ABOUT DESIGN AREA -->
    <section id="about" class="about-me-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Sign In</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-md-offstet-2">
                    <form id="contactForm" class="contact-form" method="post" action="/login">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="email">Email address:</label>
                                <input id="name" class="form-control" type="text" name="email" placeholder="Email">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="password">Password:</label>
                                <input id="password" class="form-control" type="password" name="password" placeholder="Password">
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn" type="submit">Sign In</button>
                            </div>
                            <div class="col-md-12 text-center">
                                @include ('layouts.error')
                                @include ('layouts.success')
                            </div>
                        </div>
                    </form>
                    <a href="{{ route('registration.create') }}">Create a new account</a>
                </div>
            </div>
        </div>
    </section>
    <!-- / END ABOUT DESIGN AREA -->
@endsection