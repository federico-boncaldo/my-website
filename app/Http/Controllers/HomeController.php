<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use App\Service;
use App\Quote;
use App\WorkCategory;

class HomeController extends Controller
{
    public function index()
    {
        $about = About::first();

        $services = Service::all();
        $quotes = Quote::all();
        $categories = WorkCategory::all();

        return view('home.index', compact('about', 'services', 'quotes', 'categories'));
    }
}
