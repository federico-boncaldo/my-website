<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('about.create');
    }

    public function edit(About $about)
    {
        return view('about.edit', compact('about'));
    }

    public function update(About $about)
    {
        $about->update(request(['title', 'body']));
        return redirect("/about/edit/{$about->id}")->withSuccess('About updated');
    }

    public function destroy(About $about)
    {
        $about->delete();
        return view('about.edit');
    }

    public function store()
    {
        $this->validate(
            request(), [
            'title' => 'required',
            'body'    =>    'required'
            ]
        );

        About::create(
            [
            'title' => request('title'),
            'body'    => request('body'),
            'user_id' => auth()->id()
            ]
        );

        return redirect()->home();
    }
}
