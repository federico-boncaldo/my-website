<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller
{
    public function store()
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s]+$/u|min:2',
            'lastname' => 'required|regex:/^[\pL\s]+$/u|min:2',
            'email' => 'required|email',
            'subject' => 'required|regex:/^[\pL\s]+$/u|min:2',
            'message' => 'required',
            'privacy' => 'required'
        ];

        $this->validate($request, $rules);

        Mail::to('info@federicoboncaldo.com')->send(new ContactRequest($request->all()));
        Mail::to($request->email)->send(new ContactThanks($request->name));

        return redirect()->back()->withSuccess('Thanks for contacting me.');

    }
}
