<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Service $service)
    {
        return view('services.show', compact('service'));
    }

    public function create()
    {
        return view('services.create');
    }

    public function edit(Service $service)
    {
        return view('services.edit', compact('service'));
    }

    public function destroy(Service $service)
    {
        $service->delete();
        return view('services.edit', compact('service'));
    }

    public function update(Service $service)
    {
        $service->update(request(['title', 'icon', 'body']));

        return redirect("/services/edit/{$service->id}")->withSuccess('Service updated');
    }

    public function store()
    {

        $attributes = request()->validate(
            [
            'title' => ['required', 'min:3'],
            'icon' => ['required', 'min:3'],
            'body' => ['required', 'min:3']
            ]
        );

        $attributes['user_id'] = auth()->id();

        Service::create($attributes);

        return redirect()->home();
    }
}
