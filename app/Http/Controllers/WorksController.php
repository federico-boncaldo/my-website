<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WorkCategory;
use App\Work;

class WorksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Work $work)
    {
        return view('works.show', compact('work'));
    }

    public function create($category)
    {
        $categories = WorkCategory::all();

        if (count($categories) == 0)
        {
            return redirect()->home()->withErrors(['Please create one category before adding new works.']);
        }
        return view('works.create', compact('category'));
    }

    public function edit(Work $work)
    {
        return view('works.edit', compact('work'));
    }

    public function update(Work $work)
    {
        return view('works.edit', compact('work'));
    }

    public function destroy(Work $work)
    {
        return view('works.edit', compact('work'));
    }

    public function store()
    {
        $this->validate(
            request(), [
            'title' => 'required',
            'category_id' => 'required',
            ]
        );

        Work::create(
            [
            'title' => request('title'),
            'category_id' => request('category_id'),
            'user_id' => auth()->id()
            ]
        );

        return redirect()->home();
    }
}
