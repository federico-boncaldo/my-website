<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quote;

class QuotesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Quote $quote)
    {
        return view('quotes.show', compact('quote'));
    }

    public function create()
    {
        return view('quotes.create');
    }

    public function edit(Quote $quote)
    {
        return view('quotes.edit', compact('quote'));

    }

    public function destroy(Quote $quote)
    {
        $quote->delete();
        return view('quotes.edit', compact('quote'));
    }

    public function update(Quote $quote)
    {

        $quote->update(request(['body', 'author', 'author_title']));
        return redirect("/quotes/edit/{$quote->id}")->withSuccess('Quote updated');
    }

    public function store()
    {

        $attributes = request()->validate(
            [
            'body' => ['required', 'min:3'],
            'author' => ['required', 'min:3'],
            'author_title' => ['required', 'min:3']
            ]
        );

        $attributes['user_id'] = auth()->id();

        Quote::create($attributes);

        return redirect()->home();
    }
}
