<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WorkCategory;
use App\Work;

class WorkCategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('work-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate(
            [
            'title' => ['required', 'min:3']
            ]
        );

        $attributes['user_id'] = auth()->id();

        WorkCategory::create($attributes);

        return redirect()->home();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkCategory $workCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workCategory = WorkCategory::find($id);
        return view('work-categories.edit', compact('workCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkCategory $workCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workCategory = WorkCategory::find($id);
        return view('work-categories.edit', compact('workCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\WorkCategory        $workCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $workCategory = WorkCategory::find($id);
        $workCategory->update(request(['title']));

        return redirect("/works/category/edit/{$workCategory->id}")->withSuccess('Category updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkCategory $workCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workCategory = WorkCategory::find($id);
        $workCategory->delete();
        return redirect()->home()->withSuccess('Category deleted');
    }
}
