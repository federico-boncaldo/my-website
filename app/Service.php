<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
    protected $fillable = [
        'title', 'icon', 'body', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
