<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkCategory extends Model
{
     /**
	  * The attributes that are mass assignable.
	  *
	  * @var array
	  */
    protected $fillable = [
        'title', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function works()
    {
        return $this->hasMany(Work::class, 'category_id');
    }
}
