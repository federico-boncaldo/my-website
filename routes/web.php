<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::post('/contact', 'ContactController@store');

Route::post('/about', 'AboutController@store');
Route::get('/about/create', 'AboutController@create')->name('about.create');
Route::get('/about/edit/{about}', 'AboutController@edit')->name('about.edit');
Route::patch('/about/{about}', 'AboutController@update');
Route::delete('/about/{about}', 'AboutController@destroy');

Route::post('/services', 'ServicesController@store');
Route::get('/services/create', 'ServicesController@create');
Route::get('/services/{service}', 'ServicesController@show');
Route::get('/services/edit/{service}', 'ServicesController@edit')->name('service.edit');
Route::patch('/services/{service}', 'ServicesController@update');
Route::delete('/services/{service}', 'ServicesController@destroy');

Route::post('/works/category', 'WorkCategoriesController@store');
Route::get('/works/category/create', 'WorkCategoriesController@create')->name('work-categories.create');
Route::get('/works/category/{category}', 'WorkCategoriesController@show');
Route::get('/works/category/edit/{category}', 'WorkCategoriesController@edit')->name('work-categories.edit');;
Route::patch('/works/category/{category}', 'WorkCategoriesController@update');
Route::delete('/works/category/{category}', 'WorkCategoriesController@destroy');

Route::post('/works', 'WorksController@store');
Route::get('/works/create/{category}', 'WorksController@create')->name('works.create');
Route::get('/works/{work}', 'WorksController@show');
Route::get('/works/edit/{work}', 'WorksController@edit')->name('works.edit');;
Route::patch('/works/{work}', 'WorksController@update');
Route::delete('/works/{work}', 'WorksController@destroy');

Route::post('/quotes', 'QuotesController@store');
Route::get('/quotes/create', 'QuotesController@create');
Route::get('/quotes/edit/{quote}', 'QuotesController@edit');
Route::patch('/quotes/{quote}', 'QuotesController@update');
Route::delete('/quotes/{quote}', 'QuotesController@destroy');

Route::get('/blog', 'PostController@index')->name('blog');
Route::get('/post/create', 'PostController@create');
Route::get('/post/edit/{post}', 'PostController@edit');
Route::post('/post/store', 'PostController@store');
Route::patch('/post/{post}', 'PostController@update');
Route::get('/post/{post}', 'PostController@show')->name('post');

Route::get('/register', 'RegistrationController@create')->name('registration.create');
Route::post('/register', 'RegistrationController@store');

Route::get('/login', 'SessionController@create')->name('login');
Route::post('/login', 'SessionController@store');

// best practises recommend to use a post request
Route::get('/logout', 'SessionController@destroy');
