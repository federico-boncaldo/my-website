# Personal website

The website consists of a home page where there will be showed the following sections:

* About us;
* Services;
* Works;
* Quotes;
* Contact form;

Besides the home page there is also a blog section which is still in progress.

## Functionalities

The website includes authorisation functionalities.
Once a user is registered and logged in it will possible to modify the contents of the above sections.
At the moment full CRUD functionalities are implemented for most of the sections besides the blog section.
The contact form isn't fully tested but the email functionalities are implemented.

## Requirements

The website is built with Laravel 5.7 which requires PHP 7.1.3
You can find the full list of requirements [here](https://laravel.com/docs/5.7#server-requirements)